import pathlib
import shutil
import tempfile

import msgpack
import pytest
from pytest_idem.runner import run_sls
from pytest_idem.runner import tpath_hub


@pytest.fixture(scope="function", name="hub")
def managed_tpath_hub():
    with tpath_hub() as hub:
        yield hub


async def test_failed_enter(hub):
    """
    Verify that exceptions raised in an "enter" function completely halt the program
    """
    context = hub.idem.managed.context(
        run_name="test", cache_dir=tempfile.gettempdir(), esm_plugin="fail"
    )

    with pytest.raises(RuntimeError) as e:
        async with context:
            ...
        assert "Fail to enter enforced state management" in str(e)


def test_present():
    """
    Verify that a failed `present` function doesn't overwrite previous esm data
    """
    id_ = "test_|-esm_test_state_|-esm_test_state_|-"
    present_tag = f"{id_}present"
    absent_tag = f"{id_}absent"
    expected_data = {id_: {"key": "value"}}

    cache_dir = tempfile.mkdtemp()
    cache_file = pathlib.Path(cache_dir) / "esm" / "cache" / "test.msgpack"
    local_plugin_cache = pathlib.Path(cache_dir) / "esm" / "local" / "test.msgpack"

    try:
        # Run the test so that a cache exists
        ret = run_sls(["esm.pass"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is True
        assert ret[present_tag]["old_state"] is None
        assert ret[present_tag]["new_state"] == {"key": "value"}

        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)

        # Verify that new_state ends up in ctx as old_state
        ret = run_sls(["esm.pass"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is True
        assert ret[present_tag]["old_state"] == {"key": "value"}
        assert ret[present_tag]["new_state"] == {"key": "value"}

        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)

        # After a state fails and returns no data, the old_state should still be there
        ret = run_sls(["esm.fail"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is False
        assert ret[present_tag]["new_state"] is None
        assert ret[present_tag]["old_state"] == {"key": "value"}

        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)

        # Check after a second failure just to be sure
        ret = run_sls(["esm.fail"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is False
        assert ret[present_tag]["new_state"] is None
        assert ret[present_tag]["old_state"] == {"key": "value"}

        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)

        # Run the absent state, old_state should appear in the return, but not in the cache
        ret = run_sls(["esm.clean"], cache_dir=cache_dir)
        assert absent_tag in ret
        assert ret[absent_tag]["result"] is True
        assert ret[absent_tag]["new_state"] is None
        assert ret[absent_tag]["old_state"] == {"key": "value"}

        _verify_cache(cache_file)
        _verify_cache(local_plugin_cache)

        # The next time absent is run, old_state is cleared
        ret = run_sls(["esm.clean"], cache_dir=cache_dir)
        assert absent_tag in ret
        assert ret[absent_tag]["result"] is True
        assert ret[absent_tag]["new_state"] is None
        assert ret[absent_tag]["old_state"] == None

        _verify_cache(cache_file)
        _verify_cache(local_plugin_cache)

    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)


def _verify_cache(cache_file: pathlib.Path, expected_data=None):
    assert cache_file.exists()
    with cache_file.open("rb") as fh:
        data = msgpack.load(fh)

    if expected_data is None:
        assert not data
        return

    assert data == expected_data
