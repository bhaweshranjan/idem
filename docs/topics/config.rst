===============
Config Template
===============

To save all of your CLI flags in a single config file, run the full idem CLI command that you want, with ``--config-template`` as an additional flag.
The generated config file will also include options for plugins in adjacent projects that are not necessarily exposed in the idem CLI command.

At the time of this writing, a config template will include the settings shown in the following example. Note that some settings are empty because the example command only included ``--config-template`` and no other flags.

.. code-block:: bash

    $ idem --config-template

Resulting config file:

.. code-block:: yaml

    acct:
      acct_file:
      acct_key:
      crypto_plugin: fernet
      extras:
      output_file: null
    evbus:
      serial_plugin: json
    idem:
      acct_profile: default
      cache_dir: ~/.idem/var/cache/idem
      esm_keep_cache: false
      esm_plugin: local
      esm_profile: default
      esm_serial_plugin: msgpack
      exec: ''
      exec_args: []
      log_datefmt: '%H:%M:%S'
      log_file: idem.log
      log_fmt_console: '[%(levelname)-8s] %(message)s'
      log_fmt_logfile: '%(asctime)s,%(msecs)03d [%(name)-17s][%(levelname)-8s] %(message)s'
      log_handler_options:
      log_level: warning
      log_plugin: basic
      param_sources: []
      params: ''
      pending: default
      reconciler: none
      render: jinja|yaml|replacements
      root_dir: ~/.idem
      run_name: cli
      runtime: parallel
      sls: []
      sls_sources: []
      test: false
      tree: ''
    pop_config:
      log_datefmt: '%H:%M:%S'
      log_file: idem.log
      log_fmt_console: '[%(levelname)-8s] %(message)s'
      log_fmt_logfile: '%(asctime)s,%(msecs)03d [%(name)-17s][%(levelname)-8s] %(message)s'
      log_handler_options: *id001
      log_level: warning
      log_plugin: basic
    rend:
      file: null
      output: null
      pipe: yaml


To run an idem command that uses the settings from a config file, add the ``--config`` option. The following ``idem state`` example has ``my_config.cfg`` as the saved config file.

.. code-block:: bash

    $ idem state --config=my_config.cfg
