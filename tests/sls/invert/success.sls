second:
  test.nop:
  - require:
    - test: first

third:
  test.nop:
  - require:
    - test: second

any_order:
  test.nop

first:
  test.nop
